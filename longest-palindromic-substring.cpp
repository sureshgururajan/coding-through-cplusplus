#include <bits/stdc++.h>

using namespace std;

class Solution
{
private:
    bool isPalindrome(string &s, int start, int end)
    {
        for (int i = start, j = end; i < j; i++, j--)
        {
            if (s[i] != s[j])
            {
                return false;
            }
        }
        return true;
    }

public:
    string longestPalindrome(string s)
    {
        int res{}, longestStart{}, longestEnd{};
        for (int i = 0; i < s.length(); i++)
        {
            for (int j = i; j < s.length(); j++)
            {
                if (isPalindrome(s, i, j))
                {
                    if (res < j - i + 1)
                    {
                        res = j - i + 1;
                        longestStart = i;
                        longestEnd = j + 1;
                    }
                }
            }
        }
        int k = 0;
        char result[res + 1];
        for (int i = longestStart; i < longestEnd; i++)
        {
            result[k] = s[i];
            k++;
        }
        result[res] = '\0';

        return result;
    }
};

class Solution2
{
public:
    string longestPalindrome(string s)
    {
        int start{}, end{};
        for (int i = 0; i < s.length(); i++)
        {
            int len1 = findLongestPalindrome(s, i, i);
            int len2 = findLongestPalindrome(s, i, i + 1);
            int greater = max(len1, len2);

            if (greater > end - start)
            {
                start = i - (greater - 1) / 2;
                end = i + (greater / 2);
            }
        }

        return s.substr(start, end - start + 1);
    }

    int findLongestPalindrome(string s, int start, int end)
    {
        int left = start, right = end;
        while (left >= 0 and right < s.length() and s[left] == s[right]) {
            left--;
            right++;
        }
        return right - left - 1;
    }
};

int main()
{
    string testCases[] {"babad", "cbbd", "madam", "malayalam", "suresh"};
    Solution *s = new Solution();
    for (string x: testCases) {
        cout << s -> longestPalindrome(x) << endl;
    }    
    return 0;
}
