#include <bits/stdc++.h>

using namespace std;


/*

References

 -> The & operator returns an address of the concerned variable
 -> The * operator tells the compiler that this is a datatype's pointer
 -> *b = &a means we declare a pointer and then assign it the address of variable "a"


*/


int main()
{
    int a = 10;
    int *b = &a;
    int *c;
    c = &a;
    cout << b << "\n" << *b << endl;
    cout << *c << endl;
}