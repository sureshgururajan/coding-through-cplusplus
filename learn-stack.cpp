#include <bits/stdc++.h>
using namespace std;

struct StackNode
{
    int val;
    StackNode *previous;
    StackNode *next;

    StackNode() : val(0), previous(nullptr), next(nullptr){};
    StackNode(int val) : val(val), previous(nullptr), next(nullptr){};
    StackNode(int val, StackNode *previous, StackNode *next) : val(val), previous(previous), next(next){};
};

class Stack
{
private:
    StackNode *head, *tail;
    bool initialized;

    void create(int x)
    {
        StackNode *stackNode = new StackNode(x);
        if (initialized == false)
        {
            head = stackNode;
            tail = stackNode;
            head->previous = nullptr;
            tail->previous = head;
            initialized = true;
        }
        else
        {
            tail->next = stackNode;
            stackNode->previous = tail;
            tail = tail->next;
        }
    }

public:
    void push(int x)
    {
        create(x);
    }

    int pop()
    {
        if (initialized == false)
        {
            return -1;
        }

        if (tail == head)
        {
            initialized = false;
            int tailVal = tail->val;
            delete head;
            return tailVal;
        }

        StackNode *last = tail;
        tail = tail->previous;
        int lastValue = last->val;
        delete last;
        return lastValue;
    }

    int top()
    {
        if (initialized == false) {
            return -1;
        }
        return tail->val;
    }
};

int main()
{
    Stack *stack = new Stack();
    stack->push(10);
    stack->push(20);
    stack->push(30);

    for (auto i = 0; i < 6; i++)
    {
        cout << "Top -> " << stack->top() << endl;
        cout << "Popping -> " << stack->pop() << endl;
    }

    stack->push(1);
    stack->push(2);
    stack->push(3);

    for (auto i = 0; i < 6; i++)
    {
        cout << "Top -> " << stack->top() << endl;
        cout << "Popping -> " << stack->pop() << endl;
    }

    return 0;
}