#include <bits/stdc++.h>

using namespace std;

struct QueueNode
{
    int val;
    QueueNode *next;
    QueueNode *prev;

    QueueNode() : val(0), next(nullptr), prev(nullptr){};
    QueueNode(int val) : val(val), next(nullptr), prev(nullptr){};
};

class Queue
{
private:
    QueueNode *first, *last;
    bool initialized{false};

    void enqueueHelper(int val)
    {
        QueueNode *queueNode = new QueueNode(val);
        if (!initialized)
        {
            first = queueNode;
            last = queueNode;

            first->prev = nullptr;
            first->next = last;

            last->prev = first;
            last->next = nullptr;

            initialized = true;
        }
        else
        {
            last->next = queueNode;
            last = last->next;
        }
    }

    int dequeueHelper()
    {
        if (!initialized) {
            return -1;
        }
        QueueNode *firstNode = first;
        int firstNodeValue = firstNode -> val;
        first = first->next;
        delete firstNode;
        if (!first) {
            initialized = false;
        }
        return firstNodeValue;
    }

public:
    void enqueue(int val)
    {
        enqueueHelper(val);
    }

    int dequeue()
    {
        return dequeueHelper();
    }
};

int main()
{
    Queue *queue = new Queue();
    queue->enqueue(1);
    queue->enqueue(2);
    queue->enqueue(3);
    queue->enqueue(4);

    for (int i = 0; i < 10; i++)
    {
        cout << queue->dequeue() << endl;
    }

    queue->enqueue(11);
    queue->enqueue(12);
    queue->enqueue(13);
    queue->enqueue(14);

    for (int i = 0; i < 10; i++)
    {
        cout << queue->dequeue() << endl;
    }
    return 0;
}