#include <bits/stdc++.h>

using namespace std;

// Create a structure variable called myStructure
struct DataType{
  int myNum;
  string myString;
};


struct ListNode{
  int val;
  ListNode *next;

  ListNode(): val(0), next(nullptr) {}
  ListNode(int x): val(x), next(nullptr) {}
  ListNode(int x, ListNode *next): val(x), next(next) {}
};


int main() {
    DataType dataEntry;
    dataEntry.myNum = 10;
    dataEntry.myString = "Suresh Gururajan";
    cout << dataEntry.myNum << endl;
    cout << dataEntry.myString << endl;
    return 0;
}

/*
    References

    1. https://www.w3schools.com/cpp/cpp_structs.asp
    2. https://en.cppreference.com/w/cpp/language/constructor - Initializer list
*/