#include <bits/stdc++.h>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution
{
private:
    int initialized = 0;
    ListNode *head, *tail;

public:
    void create(int digit)
    {
        ListNode *newNode = new ListNode(digit);
        if (initialized == 0)
        {
            head = newNode;
            tail = newNode;
            initialized = 1;
        }
        else
        {
            tail->next = newNode;
            tail = newNode;
        }
    }

    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
    {
        int result{}, carry{};

        while (l1 and l2)
        {
            result = (l1->val + l2->val + carry) % 10;
            carry = (l1->val + l2->val + carry) / 10;

            create(result);

            l1 = l1->next;
            l2 = l2->next;
        }

        while (l1)
        {
            result = (l1->val + carry) % 10;
            carry = (l1->val + carry) / 10;

            create(result);
            l1 = l1->next;
        }

        while (l2)
        {
            result = (l2->val + carry) % 10;
            carry = (l2->val + carry) / 10;
            create(result);
            l2 = l2->next;
        }

        if (carry)
        {
            create(carry);
        }

        return head;
    }
};

int main()
{
    Solution s;

    ListNode *n1 = new ListNode(5);
    ListNode *n2 = new ListNode(5);
    ListNode *result = s.addTwoNumbers(n1, n2);
    while (result)
    {
        cout << result->val << endl;
        result = result->next;
    }
    return 0;
}