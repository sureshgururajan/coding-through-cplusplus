#include <bits/stdc++.h>

using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class LinkedList
{
private:
    ListNode *head, *tail;

public:
    LinkedList(int x)
    {
        ListNode *newNode = new ListNode(x);
        head = newNode;
        tail = newNode;
    }

    void traverse()
    {
        ListNode *traversalHead = head;
        while (traversalHead)
        {
            cout << traversalHead->val << endl;
            traversalHead = traversalHead->next;
        }
    }

    void append(int x)
    {
        ListNode *newNode = new ListNode(x);
        tail->next = newNode;
        tail = tail->next;
    }
};

int main()
{
    LinkedList *l = new LinkedList(1);
    l->append(2);
    l->append(3);
    l->append(4);
    l->traverse();
    return 0;
}