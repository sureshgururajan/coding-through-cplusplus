#include<bits/stdc++.h>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int res{};
        for (int i = 0; i < s.length(); i++ ) {
            for (int j = i; j < s.length(); j++) {
                if (checkRepetitions(s, i, j)) {
                    res = max(res, j - i + 1);
                }
            }
        }
        return res;
    }

    bool checkRepetitions(string &s, int start, int end) {
        vector<int> chars(128);
        for (int i = start; i <= end; i++) {
            char c = s[i];
            chars[c]++;
            if (chars[c] > 1) {
                return false;
            }
        }

        return true;
    }
};


class Solution2 {
public:
    int lengthOfLongestSubstring(string s) {
        vector<int> chars(128);

        int left = 0;
        int right = 0;

        int res = 0;
        while (right < s.length()) {
            char r = s[right];
            chars[r]++;

            while (chars[r] > 1) {
                char l = s[left];
                chars[l]--;
                left++;
            }

            res = max(res, right - left + 1);

            right++;
        }

        return res;
    }
};

int main() {
    Solution2 *solution = new Solution2();
    cout << solution -> lengthOfLongestSubstring("sureshgururajan") << endl;
    return 0;
}