# How to use latest C++ compiler

See https://stackoverflow.com/questions/63007678/how-do-i-upgrade-to-c17

```
g++ -std=c++17 hello.cpp
```

# References

## Vectors

1. https://www.geeksforgeeks.org/sorting-a-vector-in-c/
1. https://www.w3schools.com/cpp/cpp_classes.asp
1. https://www.geeksforgeeks.org/binary-search-functions-in-c-stl-binary_search-lower_bound-and-upper_bound/
1. https://www.geeksforgeeks.org/vector-in-cpp-stl/


## Initializer Lists

1. https://en.cppreference.com/w/cpp/language/constructor

## Classes and Structs

1. https://stackoverflow.com/questions/1127396/struct-constructor-in-c

> In C++ the only difference between a class and a struct is that members and base classes are private by default in classes, whereas they are public by default in structs. 

