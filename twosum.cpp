#include <bits/stdc++.h>

using namespace std;

class Solution
{
public:
    vector<int> twoSum(vector<int> &nums, int target)
    {
        vector<int> answer;
        map<int, int> numberToIndexMap;

        for (int i = 0; i < nums.size(); ++i)
        {
            int number = nums.at(i);
            int expected = target - number;

            if (numberToIndexMap.find(expected) != numberToIndexMap.end())
            {
                answer.push_back(i);
                answer.push_back(numberToIndexMap[expected]);
                return answer;
            }
            else
            {
                numberToIndexMap[number] = i;
            }
        }
        return answer;
    }
};

int main()
{
    vector<int> input{3, 2, 4};

    Solution s;
    vector<int> output = s.twoSum(input, 6);
    cout << output.at(0) << "; " << output.at(1) << endl;

    return 0;
}

/*

References
1. https://www.geeksforgeeks.org/sorting-a-vector-in-c/
2. https://www.w3schools.com/cpp/cpp_classes.asp
3. https://www.geeksforgeeks.org/binary-search-functions-in-c-stl-binary_search-lower_bound-and-upper_bound/
4. https://www.geeksforgeeks.org/vector-in-cpp-stl/
5. https://stackoverflow.com/questions/3578083/what-is-the-best-way-to-use-a-hashmap-in-c
6.
*/